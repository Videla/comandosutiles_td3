# Comandos útiles TD3

---

Inicio comandos tomados desde:  
<https://www.youtube.com/watch?v=3NTXFUxcKPc&t=4s>

---

## Hexdump

It has ability to dump file contents into many formats like hexadecimal, octal, ASCII and decimal. This command takes a file, or any standard input, as input parameter and converts it to the format of your choice. Let's assume you work with binary data and you are unable to understand the format of a file, you can make use of Hexdump command to get file contents in much better readable format.

<https://linoxide.com/linux-how-to/linux-hexdump-command-examples/>

Para hacer un hexdump de un archivo con un formato alineado:

``` sh
$ hexdump -C Linoxide
00000000  54 68 69 73 20 69 73 20  20 61 20 74 65 73 74 20  |This is  a test |
00000010  4c 69 6e 6f 78 69 64 65  20 46 69 6c 65 0a 55 73  |Linoxide File.Us|
00000020  65 64 20 66 6f 72 20 64  65 6d 6f 6e 73 74 72 61  |ed for demonstra|
00000030  74 69 6f 6e 20 70 75 72  70 6f 73 65 73 0a 0a     |tion purposes..|
0000003f
```

### Parámetros de hexdump

-C  
Canonical hex+ASCII display.  Display the input offset in hexadecimal, followed by sixteen space-separated, two column, hexadecimal bytes, followed by the same sixteen bytes in %_p format enclosed in ``|'' characters.

Calling the command hd implies this option.

<https://www.freebsd.org/cgi/man.cgi?query=hexdump&sektion=1>

### Imprimir todos los strings imprimibles de un archivo

Es decir, si tenemos un archivo tipo ELF, nos imprimie todo lo que tenga sentido como string, que seran algunas librerias y palabras imprimibles aisladas.

``` sh
strings archivo
```

## Objdump

objdump is a command-line program for displaying various information about object files on Unix-like operating systems. For instance, it can be used as a disassembler to view an executable in assembly form. It is part of the GNU Binutils for fine-grained control over executables and other binary data.

<https://en.wikipedia.org/wiki/Objdump>

### Para hacer un object dump en lugar de un disassembly

``` sh
objdump -d archivo
```

-d  
--disassemble  
--disassemble=symbol  
Display the assembler mnemonics for the machine instructions from the input file. This option only disassembles those sections which are expected to contain instructions. If the optional symbol argument is given, then display the assembler mnemonics starting at symbol. If symbol is a function name then disassembly will stop at the end of the function, otherwise it will stop when the next symbol is encountered. If there are no matches for symbol then nothing will be displayed.

<https://sourceware.org/binutils/docs/binutils/objdump.html>

### Otra forma de usar objectdump

``` sh
objdump -x archivo
```

### Y para navegarlo de forma sencilla

``` sh
objdump -x archivo | less
```

Esta forma nos dara una especie de indice con secciones y demas

### Parámetros para objectdump

``` sh
-CprsSx --prefix-addresses

-C
--demangle[=style]
Decode (demangle) low-level symbol names into user-level names. Besides removing any initial underscore prepended by the system, this makes C++ function names readable. Different compilers have different mangling styles. The optional demangling style argument can be used to choose an appropriate demangling style for your compiler. See c++filt, for more information on demangling.

-p
--private-headers
Print information that is specific to the object file format. The exact information printed depends upon the object file format. For some object file formats, no additional information is printed.

-r
--reloc
Print the relocation entries of the file. If used with -d or -D, the relocations are printed interspersed with the disassembly.

-s
--full-contents
Display the full contents of any sections requested. By default all non-empty sections are displayed.

-S
--source
Display source code intermixed with disassembly, if possible. Implies -d.

-x
--all-headers
Display all available header information, including the symbol table and relocation entries. Using -x is equivalent to specifying all of -a -f -h -p -r -t.








-a
--archive-header
If any of the objfile files are archives, display the archive header information (in a format similar to ‘ls -l’). Besides the information you could list with ‘ar tv’, ‘objdump -a’ shows the object file format of each archive member.

-f
--file-headers
Display summary information from the overall header of each of the objfile files.

-h
--section-headers
--headers
Display summary information from the section headers of the object file.

File segments may be relocated to nonstandard addresses, for example by using the -Ttext, -Tdata, or -Tbss options to ld. However, some object file formats, such as a.out, do not store the starting address of the file segments. In those situations, although ld relocates the sections correctly, using ‘objdump -h’ to list the file section headers cannot show the correct addresses. Instead, it shows the usual addresses, which are implicit for the target.

Note, in some cases it is possible for a section to have both the READONLY and the NOREAD attributes set. In such cases the NOREAD attribute takes precedence, but objdump will report both since the exact setting of the flag bits might be important.

-t
--syms
Print the symbol table entries of the file. This is similar to the information provided by the ‘nm’ program, although the display format is different. The format of the output depends upon the format of the file being dumped, but there are two main types. One looks like this:

[  4](sec  3)(fl 0x00)(ty   0)(scl   3) (nx 1) 0x00000000 .bss
[  6](sec  1)(fl 0x00)(ty   0)(scl   2) (nx 0) 0x00000000 fred
where the number inside the square brackets is the number of the entry in the symbol table, the sec number is the section number, the fl value are the symbol’s flag bits, the ty number is the symbol’s type, the scl number is the symbol’s storage class and the nx value is the number of auxilary entries associated with the symbol. The last two fields are the symbol’s value and its name.

The other common output format, usually seen with ELF based files, looks like this:

00000000 l    d  .bss   00000000 .bss
00000000 g       .text  00000000 fred
Here the first number is the symbol’s value (sometimes refered to as its address). The next field is actually a set of characters and spaces indicating the flag bits that are set on the symbol. These characters are described below. Next is the section with which the symbol is associated or *ABS* if the section is absolute (ie not connected with any section), or *UND* if the section is referenced in the file being dumped, but not defined there.

After the section name comes another field, a number, which for common symbols is the alignment and for other symbol is the size. Finally the symbol’s name is displayed.

The flag characters are divided into 7 groups as follows:

l
g
u
!
The symbol is a local (l), global (g), unique global (u), neither global nor local (a space) or both global and local (!). A symbol can be neither local or global for a variety of reasons, e.g., because it is used for debugging, but it is probably an indication of a bug if it is ever both local and global. Unique global symbols are a GNU extension to the standard set of ELF symbol bindings. For such a symbol the dynamic linker will make sure that in the entire process there is just one symbol with this name and type in use.

w
The symbol is weak (w) or strong (a space).

C
The symbol denotes a constructor (C) or an ordinary symbol (a space).

W
The symbol is a warning (W) or a normal symbol (a space). A warning symbol’s name is a message to be displayed if the symbol following the warning symbol is ever referenced.

I
i
The symbol is an indirect reference to another symbol (I), a function to be evaluated during reloc processing (i) or a normal symbol (a space).

d
D
The symbol is a debugging symbol (d) or a dynamic symbol (D) or a normal symbol (a space).

F
f
O
The symbol is the name of a function (F) or a file (f) or an object (O) or just a normal symbol (a space).

--prefix-addresses
When disassembling, print the complete address on each line. This is the older disassembly format.
```

<https://sourceware.org/binutils/docs/binutils/objdump.html>

#### Una herramienta util es strace

Podemos ejecutar nuestro programa colocando antes strace
strace ./programa

#### Otra herramienta util es ltrace

se ejecuta igual que la anterior

#### para abrir un archivo con radare2

r2 archivo

---

Fin comandos tomados desde:  
<https://www.youtube.com/watch?v=3NTXFUxcKPc&t=4s>

---

#### Comandos de radare

``` sh
aaa
afl
?       Ayuda
a?      Nos devuelve que otras letras podemos agregar a la a
s       cambia la ubicacion, admite completar con tab
pdf     imprime el disassembly de la funcion actual, con flechas que muestran los saltos del codigo.
vv      entra al modo visual, dentro del mismo:
        tab y shift tab selecciona bloques, despues con los comandos de movimiento de vim lo podemos mover.
p       nos muestra diferentes representaciones
```

#### para debuggear un programa con radare como se hace con gdb

``` sh
rd -d archivo
```

#### Comandos para debuggear con radare

``` sh
s sym.main -> ir a la funcion main.
aaa
pdf
db "direccion de memoria" -> coloca un breakpoint en la direccion de memoria.
VV      -> nos lleva al modo visual
:       -> nos lleva al modo comando para meter comandos
:dc     -> corre el programa
s       -> da pasos entre instrucciones
S       -> hace lo mismo pero no sigue funciones que no queremos seguir (?)
```

## Comandos Bochs

### Agregado desde Notas_Bochs.md, dicho archivo se elimino luego

## Este comando muestra la ayuda

``` sh
<bochs:1> h
h|help - show list of debugger commands
h|help command - show short command description
-*- Debugger control -*-
    help, q|quit|exit, set, instrument, show, trace, trace-reg,
    trace-mem, u|disasm, ldsym, slist
-*- Execution control -*-
    c|cont|continue, s|step, p|n|next, modebp, vmexitbp
-*- Breakpoint management -*-
    vb|vbreak, lb|lbreak, pb|pbreak|b|break, sb, sba, blist,
    bpe, bpd, d|del|delete, watch, unwatch
-*- CPU and memory contents -*-
    x, xp, setpmem, writemem, crc, info,
    r|reg|regs|registers, fp|fpu, mmx, sse, sreg, dreg, creg,
    page, set, ptime, print-stack, ?|calc
-*- Working with bochs param tree -*-
    show "param", restore
```

## List of CPU integer registers and their contents

<http://bochs.sourceforge.net/doc/docbook/user/internal-debugger.html>

``` sh
<bochs:2> regs
CPU0:
rax: 00000000_00000000 rcx: 00000000_00000000
rdx: 00000000_00000000 rbx: 00000000_00000000
rsp: 00000000_00000000 rbp: 00000000_00000000
rsi: 00000000_00000000 rdi: 00000000_00000000
r8 : 00000000_00000000 r9 : 00000000_00000000
r10: 00000000_00000000 r11: 00000000_00000000
r12: 00000000_00000000 r13: 00000000_00000000
r14: 00000000_00000000 r15: 00000000_00000000
rip: 00000000_0000fff0
eflags 0x00000002: id vip vif ac vm rf nt IOPL=0 of df if tf sf zf af pf cf
```

Puede verse que los registros que muestra aca (excepto los que aparecen en y despues de eflags, que no los encontre) son los que aparecen en eeste link:
<http://wiki.electron.frba.utn.edu.ar/doku.php?id=td3:guiasupervivenciaasm#registros_de_la_familia_ia>  
En la seccion de registros de 64b.

## Show segment registers and their contents

<http://bochs.sourceforge.net/doc/docbook/user/internal-debugger.html>

``` sh
<bochs:3> sreg
es:0x0000, dh=0x00009300, dl=0x0000ffff, valid=7
        Data segment, base=0x00000000, limit=0x0000ffff, Read/Write, Accessed
cs:0xf000, dh=0xff0093ff, dl=0x0000ffff, valid=7
        Data segment, base=0xffff0000, limit=0x0000ffff, Read/Write, Accessed
ss:0x0000, dh=0x00009300, dl=0x0000ffff, valid=7
        Data segment, base=0x00000000, limit=0x0000ffff, Read/Write, Accessed
ds:0x0000, dh=0x00009300, dl=0x0000ffff, valid=7
        Data segment, base=0x00000000, limit=0x0000ffff, Read/Write, Accessed
fs:0x0000, dh=0x00009300, dl=0x0000ffff, valid=7
        Data segment, base=0x00000000, limit=0x0000ffff, Read/Write, Accessed
gs:0x0000, dh=0x00009300, dl=0x0000ffff, valid=7
        Data segment, base=0x00000000, limit=0x0000ffff, Read/Write, Accessed
ldtr:0x0000, dh=0x00008200, dl=0x0000ffff, valid=1
tr:0x0000, dh=0x00008b00, dl=0x0000ffff, valid=1
gdtr:base=0x0000000000000000, limit=0xffff
idtr:base=0x0000000000000000, limit=0xffff
```

Hasta DS incluido son todos registros que aparecen en
<http://wiki.electron.frba.utn.edu.ar/doku.php?id=td3:guiasupervivenciaasm#registros_de_la_familia_ia>  
en la parte de 16 bits, luego, todos los anteriores mas fs y gs pertenecen a la parte de 32 bits.
Los ultimos 4 son punteros a tablas del sistema y pertenecen tanto a 32 como a 16 bits.

### Para hacer un volcado a memoria

``` sh
x /16 0x00000000
u /16 0x00000000
```

Donde
x es en formato binario
u es en un formato mas legible, haciendo disassembly
/16 es la cantidad de bytes que quiero mirar.
0x00000000 es la direccion de memoria a partir de la cual queremos imprimir la cantidad anterior de bytes.

#### Para ver la GDT

``` sh
info gdt
```

#### Readelf

Un comando muy util es readelf, ya que muestra mucha informacion de los archivos tipo elf.
